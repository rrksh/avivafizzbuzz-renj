﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BuzzRule.cs" company="TCS">
//    Copyright (c) Tata consultancy Services. All rights reserved.
// </copyright>
// <summary>
//   Defines the BuzzRule type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FizzBuzz.Business.Implementation
{
    using System;

    using FizzBuzz.Business.Interfaces;

    /// <summary>
    /// The buzz rule.
    /// </summary>
    public class BuzzRule : IRule
    {
        /// <summary>
        /// The sequence name.
        /// </summary>
        private const string Buzz = "Buzz";

        /// <summary>
        /// The display.
        /// </summary>
        private const string Wuzz = "Wuzz";

        /// <summary>
        /// The get sequence name.
        /// </summary>
        /// <param name="day">
        /// The day.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetSequenceName(DayOfWeek day)
        {
            return day != DayOfWeek.Wednesday ? Buzz : Wuzz;
        }

        /// <summary>
        /// The is divisible.
        /// </summary>
        /// <param name="count">
        /// The count.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsDivisible(int count)
        {
            return count % 5 == 0;
        }
    }
}
