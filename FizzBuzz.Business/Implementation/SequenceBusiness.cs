﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SequenceBusiness.cs" company="TCS">
//   Copyright (c) Tata consultancy Services. All rights reserved.
// </copyright>
// <summary>
//   Defines the SequenceBusiness type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FizzBuzz.Business.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using FizzBuzz.Business.Interfaces;

    using Microsoft.Practices.ObjectBuilder2;

    /// <summary>
    /// The sequence business.
    /// </summary>
    public class SequenceBusiness : ISequenceBusiness
    {
        #region Private Variables

        /// <summary>
        /// The rules.
        /// </summary>
        private readonly IEnumerable<IRule> rules;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SequenceBusiness"/> class.
        /// </summary>
        /// <param name="rules">
        /// The fizz Buzz.
        /// </param>
        public SequenceBusiness(IRule[] rules)
        {
            this.rules = rules;
        }

        #endregion

        /// <summary>
        /// Gets the current day.
        /// </summary>
        private static DayOfWeek CurrentDay
        {
            get
            {
                return DateTime.Now.DayOfWeek;
            }
        }

        /// <summary>
        /// The get sequence.
        /// </summary>
        /// <param name="number">
        /// The number.
        /// </param>
        /// <returns>
        /// The sequence collection
        /// </returns>
        public List<string> GetSequence(int number)
        {
            var sequence = new List<string>();

            Enumerable.Range(1, number).ForEach(
                index =>
                {
                    var fizzbuzz = new StringBuilder();

                    this.rules
                    .Where(rule => rule.IsDivisible(index))
                    .ForEach(
                        rule =>
                        {
                            fizzbuzz
                            .Append(rule.GetSequenceName(CurrentDay))
                            .Append(" ");
                        });

                    sequence.Add(fizzbuzz.Length > 0 ? fizzbuzz.ToString() : index.ToString());
                });

            return sequence;
        }
    }
}
