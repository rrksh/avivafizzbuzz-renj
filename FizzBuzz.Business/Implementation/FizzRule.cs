﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FizzRule.cs" company="TCS">
//    Copyright (c) Tata consultancy Services. All rights reserved.
// </copyright>
// <summary>
//   Defines the FizzRule type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FizzBuzz.Business.Implementation
{
    using System;

    using FizzBuzz.Business.Interfaces;

    /// <summary>
    /// The fizz rule.
    /// </summary>
    public class FizzRule : IRule
    {
        /// <summary>
        /// The fizz.
        /// </summary>
        private const string Fizz = "Fizz";

        /// <summary>
        /// The display.
        /// </summary>
        private const string Wizz = "Wizz";

        /// <summary>
        /// The get sequence name.
        /// </summary>
        /// <param name="day">
        /// The day.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetSequenceName(DayOfWeek day)
        {
            return day != DayOfWeek.Wednesday ? Fizz : Wizz;
        }

        /// <summary>
        /// The is divisible.
        /// </summary>
        /// <param name="count">
        /// The count.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsDivisible(int count)
        {
            return count % 3 == 0;
        }
    }
}
