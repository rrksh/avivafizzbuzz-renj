﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRule.cs" company="TCS">
//    Copyright (c) Tata consultancy Services. All rights reserved.
// </copyright>
// <summary>
//   Defines the IRule type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FizzBuzz.Business.Interfaces
{
    using System;

    /// <summary>
    /// The Rule interface.
    /// </summary>
    public interface IRule
    {
        /// <summary>
        /// The get sequence name.
        /// </summary>
        /// <param name="day">
        /// The day.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetSequenceName(DayOfWeek day);

        /// <summary>
        /// The is divisible.
        /// </summary>
        /// <param name="count">
        /// The count.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool IsDivisible(int count);
    }
}
