﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISequenceBusiness.cs" company="TCS">
//   Copyright (c) Tata consultancy Services. All rights reserved.
// </copyright>
// <summary>
//   Defines the ISequenceBusiness type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FizzBuzz.Business
{
    using System.Collections.Generic;

    /// <summary>
    /// The SequenceBusiness interface.
    /// </summary>
    public interface ISequenceBusiness
    {
        /// <summary>
        /// The get sequence.
        /// </summary>
        /// <param name="number">
        /// The number.
        /// </param>
        /// <returns>
        /// The sequence collection
        /// </returns>
        List<string> GetSequence(int number);
    }
}
