﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SequenceController.cs" company="TCS">
//    Copyright (c) Tata consultancy Services. All rights reserved.
// </copyright>
// <summary>
//   Defines the SequenceController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FizzBuzz.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Web.Mvc;

    using FizzBuzz.Business;
    using FizzBuzz.Web.Models;
    using FizzBuzz.Web.Utility;

    using PagedList;

    /// <summary>
    /// The sequence controller.
    /// </summary>
    public class SequenceController : Controller
    {
        /// <summary>
        /// The sequence number.
        /// </summary>
        private const string SequenceNumber = "SequenceNumber";

        /// <summary>
        /// Sequence service 
        /// </summary>
        private readonly ISequenceBusiness sequenceBusiness;

        /// <summary>
        /// The maximum page.
        /// </summary>
        private readonly int maximumPage;

        /// <summary>
        /// The minimum page.
        /// </summary>
        private readonly int minimumPage;

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SequenceController" /> class.
        /// </summary>
        /// <param name="sequenceBusiness">a sequence business object</param>
        public SequenceController(ISequenceBusiness sequenceBusiness)
        {
            this.sequenceBusiness = sequenceBusiness;
            this.maximumPage = Convert.ToInt32(ConfigurationManager.AppSettings["MaximumPage"]);
            this.minimumPage = Convert.ToInt32(ConfigurationManager.AppSettings["MinimumPage"]);
        }

        #endregion

        /// <summary>
        /// The index.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Index()
        {
            var model = this.BuildViewModel(new List<string>(), this.minimumPage);
            return this.View("Index", model);
        }

        /// <summary>
        /// The fizz buzz.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FizzBuzz(FizzBuzzSequence model)
        {
            if (this.ModelState.IsValid)
            {
                var fizzbuzzList = this.sequenceBusiness.GetSequence(model.SequenceNumber);

                Helper.SetDataToCache(fizzbuzzList, SequenceNumber);

                var viewModel = this.BuildViewModel(fizzbuzzList, this.minimumPage);

                return this.View("Index", viewModel);
            }

            return this.View("Index", model);
        }

        /// <summary>
        /// The get page.
        /// </summary>
        /// <param name="page">
        /// The page.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult GetPage(int page)
        {
            var sequenceList = Helper.GetDataFromCache(SequenceNumber);

            if (sequenceList == null)
            {
                return this.RedirectToAction("Index");
            }

            var model = this.BuildViewModel(sequenceList, page);

            return this.View("Index", model);
        }

        #region private methods

        /// <summary>
        /// The build view model.
        /// </summary>
        /// <param name="fizzbuzzList">
        /// The fizz buzz list.
        /// </param>
        /// <param name="page">
        /// The page.
        /// </param>
        /// <returns>
        /// The <see cref="FizzBuzzSequence"/>.
        /// </returns>
        private FizzBuzzSequence BuildViewModel(IEnumerable<string> fizzbuzzList, int page)
        {
            var model = new FizzBuzzSequence { SequenceNumberList = fizzbuzzList.ToPagedList(page, this.maximumPage) };
            return model;
        }

        #endregion
    }
}
