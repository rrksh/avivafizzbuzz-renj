﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Helper.cs" company="TCS">
//    Copyright (c) Tata consultancy Services. All rights reserved.
// </copyright>
// <summary>
//   Defines the Helper type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FizzBuzz.Web.Utility
{
    using System.Collections.Generic;
    using System.Web;

    /// <summary>
    /// The helper.
    /// </summary>
    public static class Helper
    {
        /// <summary>
        /// The set data to cache.
        /// </summary>
        /// <param name="fizzbuzzList">
        /// The fizz buzz list.
        /// </param>
        /// <param name="cacheKeySequencenumber">
        /// The cache key sequence number.
        /// </param>
        public static void SetDataToCache(List<string> fizzbuzzList, string cacheKeySequencenumber)
        {
            if (HttpContext.Current.Cache[cacheKeySequencenumber] != null)
            {
                RemoveCache(cacheKeySequencenumber);
            }

            HttpContext.Current.Cache.Insert(cacheKeySequencenumber, fizzbuzzList);
        }

        /// <summary>
        /// The get data from cache.
        /// </summary>
        /// <param name="cacheKeySequencenumber">
        /// The cache key sequence number.
        /// </param>
        /// <returns>
        /// The sequence collection.
        /// </returns>
        public static List<string> GetDataFromCache(string cacheKeySequencenumber)
        {
            if (HttpContext.Current.Cache[cacheKeySequencenumber] != null)
            {
                return (List<string>)HttpContext.Current.Cache[cacheKeySequencenumber];
            }

            return null;
        }

        /// <summary>
        /// The remove cache.
        /// </summary>
        /// <param name="cacheKeySequencenumber">
        /// The cache key sequence number.
        /// </param>
        private static void RemoveCache(string cacheKeySequencenumber)
        {
            HttpContext.Current.Cache.Remove(cacheKeySequencenumber);
        }
    }
}