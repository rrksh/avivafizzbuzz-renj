﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FizzBuzzSequence.cs" company="TCS">
//    Copyright (c) Tata consultancy Services. All rights reserved.
// </copyright>
// <summary>
//   Defines the FizzBuzzSequence type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FizzBuzz.Web.Models
{
    using System.ComponentModel.DataAnnotations;

    using PagedList;

    /// <summary>
    /// The fizz buzz sequence.
    /// </summary>
    public class FizzBuzzSequence
    {
        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        [Required(ErrorMessage = "Please Enter a Sequence Number")]
        [Range(1, 1000, ErrorMessage = "Value must be between 1 and 1000")]
        [Display(Name = "Enter Sequence Number")]
        public int SequenceNumber { get; set; }

        /// <summary>
        /// Gets or sets the sequence number list.
        /// </summary>
        public IPagedList<string> SequenceNumberList { get; set; }
    }
}