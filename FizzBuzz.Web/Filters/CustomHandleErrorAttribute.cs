﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomHandleErrorAttribute.cs" company="TCS">
//    Copyright (c) Tata consultancy Services. All rights reserved. 
// </copyright>
// <summary>
//   Defines the CustomHandleErrorAttribute type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FizzBuzz.Web.Filters
{
    using System.Web.Mvc;

    /// <summary>
    /// The custom handle error attribute.
    /// </summary>
    public class CustomHandleErrorAttribute : HandleErrorAttribute
    {
        /// <summary>
        /// The on exception.
        /// </summary>
        /// <param name="filterContext">
        /// The filter context.
        /// </param>
        [HandleError(View = "Error")]
        public override void OnException(ExceptionContext filterContext)
        {
            // Error logs here
        }
    }
}