﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BuzzRuleTest.cs" company="TCS">
//    Copyright (c) Tata consultancy Services. All rights reserved.
// </copyright>
// <summary>
//   Defines the BuzzRuleTest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FizzBuzz.Business.Test
{
    using System;

    using FizzBuzz.Business.Implementation;
    using FizzBuzz.Business.Interfaces;

    using NUnit.Framework;

    /// <summary>
    /// The buzz rule test.
    /// </summary>
    [TestFixture]
    public class BuzzRuleTest
    {
        /// <summary>
        /// The  rules.
        /// </summary>
        private IRule rules;

        #region Test Methods
        /// <summary>
        /// The buzz_ divisible rule_ true_ test.
        /// </summary>
        [Test]
        public void Buzz_DivisibleRule_True_Test()
        {
            var result = this.rules.IsDivisible(5);

            Assert.IsTrue(result);
        }

        /// <summary>
        /// The buzz_ divisible rule_ false_ test.
        /// </summary>
        [Test]
        public void Buzz_DivisibleRule_False_Test()
        {
            var result = this.rules.IsDivisible(1);

            Assert.IsFalse(result);
        }

        /// <summary>
        /// The buzz_ display rule_Pass_test.
        /// </summary>
        [Test]
        public void Buzz_DisplayRule_Pass_Test()
        {
            var result = this.rules.GetSequenceName(DayOfWeek.Sunday);

            Assert.IsNotNull(result, "Results are null,Expected to get non nullable object");
            Assert.AreEqual(result, "Buzz");
        }

        /// <summary>
        /// The buzz_ display rule_ fail_ test.
        /// </summary>
        [Test]
        public void Buzz_DisplayRule_Fail_Test()
        {
            var result = this.rules.GetSequenceName(DayOfWeek.Wednesday);

            Assert.IsNotNull(result, "Results are null,Expected to get non nullable object");
            Assert.AreNotEqual(result, "Buzz");
        }

        /// <summary>
        /// The wuzz_ display rule_pass_test.
        /// </summary>
        [Test]
        public void Wuzz_DisplayRule_Pass_Test()
        {
            var result = this.rules.GetSequenceName(DayOfWeek.Wednesday);

            Assert.IsNotNull(result, "Results are null,Expected to get non nullable object");
            Assert.AreEqual(result, "Wuzz");
        }

        /// <summary>
        /// The wuzz_ display rule_ fail_ test.
        /// </summary>
        [Test]
        public void Wuzz_DisplayRule_Fail_Test()
        {
            var result = this.rules.GetSequenceName(DayOfWeek.Sunday);

            Assert.IsNotNull(result, "Results are null,Expected to get non nullable object");
            Assert.AreNotEqual(result, "Wuzz");
        }


        #endregion

        #region Test Initialization
        /// <summary>
        /// The Initialize.
        /// </summary>
        [SetUp]
        public void Initialize()
        {
            this.rules = new BuzzRule();
        }
        #endregion
    }
}
