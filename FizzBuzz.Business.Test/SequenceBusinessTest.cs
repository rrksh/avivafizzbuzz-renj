﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SequenceBusinessTest.cs" company="TCS">
//   Copyright (c) Tata consultancy Services. All rights reserved.
// </copyright>
// <summary>
//   Defines the SequenceBusinessTest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FizzBuzz.Business.Test
{
    using System;

    using FizzBuzz.Business.Implementation;
    using FizzBuzz.Business.Interfaces;

    using Moq;

    using NUnit.Framework;

    /// <summary>
    /// The sequence business test.
    /// </summary>
    [TestFixture]
    public class SequenceBusinessTest
    {
        /// <summary>
        /// The test target service.
        /// </summary>
        private ISequenceBusiness sequenceBusiness;

        #region Test Methods

        /// <summary>
        /// Test method for service method
        /// </summary>
        [Test]
        public void BusinessGetSequence_Pass_Test()
        {
            var result = this.sequenceBusiness.GetSequence(5);

            Assert.IsNotNull(result, "Results are null,Expected to get non nullable object");
            Assert.That(result.Count, Is.GreaterThan(0));
        }

        /// <summary>
        /// The business get sequence_ fail_ test.
        /// </summary>
        [Test]
        public void BusinessGetSequence_Fail_Test()
        {
            var result = this.sequenceBusiness.GetSequence(0);

            Assert.IsNotNull(result, "Results are null,Expected to get non nullable object");
            Assert.AreNotEqual(result.Count, Is.GreaterThan(0));
        }

        #endregion

        #region Test Initialization
        /// <summary>
        /// The Initialize.
        /// </summary>
        [SetUp]
        public void Initialize()
        {
            var mockFizz = new Mock<IRule>();
            mockFizz.Setup(x => x.IsDivisible(3)).Returns(true);
            mockFizz.Setup(x => x.GetSequenceName(It.IsAny<DayOfWeek>())).Returns("Fizz");

            var mockBuzz = new Mock<IRule>();
            mockBuzz.Setup(x => x.IsDivisible(5)).Returns(true);
            mockBuzz.Setup(x => x.GetSequenceName(It.IsAny<DayOfWeek>())).Returns("Buzz");

            var rules1 = new IRule[2];
            rules1[0] = mockFizz.Object;
            rules1[1] = mockBuzz.Object;

            this.sequenceBusiness = new SequenceBusiness(rules1);
        }

        #endregion
    }
}
