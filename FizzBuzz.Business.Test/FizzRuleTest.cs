﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FizzRuleTest.cs" company="TCS">
//    Copyright (c) Tata consultancy Services. All rights reserved.
// </copyright>
// <summary>
//   Defines the FizzRuleTest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FizzBuzz.Business.Test
{
    using System;

    using FizzBuzz.Business.Implementation;
    using FizzBuzz.Business.Interfaces;

    using NUnit.Framework;

    /// <summary>
    /// The fizz rule test.
    /// </summary>
    [TestFixture]
    public class FizzRuleTest
    {
        /// <summary>
        /// The  rules.
        /// </summary>
        private IRule rules;

        #region Test Methods

        /// <summary>
        /// The fizz_ divisible rule_ true_ test.
        /// </summary>
        [Test]
        public void Fizz_DivisibleRule_True_Test()
        {
            var result = this.rules.IsDivisible(3);

            Assert.IsTrue(result);
        }

        /// <summary>
        /// The fizz_ divisible rule_ false_ test.
        /// </summary>
        [Test]
        public void Fizz_DivisibleRule_False_Test()
        {
            var result = this.rules.IsDivisible(1);

            Assert.IsFalse(result);
        }

        /// <summary>
        /// The fizz_ display rule_ Pass_test.
        /// </summary>
        [Test]
        public void Fizz_DisplayRule_Pass_Test()
        {
            var result = this.rules.GetSequenceName(DayOfWeek.Sunday);

            Assert.IsNotNull(result, "Results are null,Expected to get non nullable object");
            Assert.AreEqual(result, "Fizz");
        }

        /// <summary>
        /// The fizz_ display rule_ fail_ test.
        /// </summary>
        [Test]
        public void Fizz_DisplayRule_Fail_Test()
        {
            var result = this.rules.GetSequenceName(DayOfWeek.Wednesday);

            Assert.IsNotNull(result, "Results are null,Expected to get non nullable object");
            Assert.AreNotEqual(result, "Fizz");
        }

        /// <summary>
        /// The wizz_ display rule_Pass_ test.
        /// </summary>
        [Test]
        public void Wizz_DisplayRule_Pass_Test()
        {
            var result = this.rules.GetSequenceName(DayOfWeek.Wednesday);

            Assert.IsNotNull(result, "Results are null,Expected to get non nullable object");
            Assert.AreEqual(result, "Wizz");
        }

        /// <summary>
        /// The wizz_ display rule_ fail_ test.
        /// </summary>
        [Test]
        public void Wizz_DisplayRule_Fail_Test()
        {
            var result = this.rules.GetSequenceName(DayOfWeek.Sunday);

            Assert.IsNotNull(result, "Results are null,Expected to get non nullable object");
            Assert.AreNotEqual(result, "Wizz");
        }

        #endregion

        #region Test Initialization
        /// <summary>
        /// The Initialize.
        /// </summary>
        [SetUp]
        public void Initialize()
        {
            this.rules = new FizzRule();
        }

        #endregion
    }
}
