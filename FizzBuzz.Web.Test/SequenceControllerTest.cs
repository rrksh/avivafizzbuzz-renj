﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SequenceControllerTest.cs" company="TCS">
//    Copyright (c) Tata consultancy Services. All rights reserved.
// </copyright>
// <summary>
//   Defines the SequenceControllerTest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FizzBuzz.Web.Test
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Web;
    using System.Web.Mvc;

    using FizzBuzz.Business;
    using FizzBuzz.Web.Controllers;
    using FizzBuzz.Web.Models;

    using Moq;

    using NUnit.Framework;

    /// <summary>
    /// The sequence controller test.
    /// </summary>
    [TestFixture]
    public class SequenceControllerTest
    {
        /// <summary>
        /// The sequence controller.
        /// </summary>
        private SequenceController sequenceController;

        /// <summary>
        /// Mock service
        /// </summary>
        private Mock<ISequenceBusiness> mockSequenceBusiness;

        /// <summary>
        /// The controller index test.
        /// </summary>
        [Test]
        public void IndexGet_Pass_Test()
        {
            var result = this.sequenceController.Index();

            Assert.IsInstanceOf(typeof(ViewResult), result);
        }

        /// <summary>
        /// The index get_ fail_ test.
        /// </summary>
        [Test]
        public void IndexGet_Fail_Test()
        {
            var result = this.sequenceController.Index() as ViewResult;

            Assert.IsNotNull(result, "Results are null,Expected to get non nullable object");
            Assert.AreNotEqual("Home", result.ViewName);
        }

        /// <summary>
        /// The test_ sequence controller_ constructor_Test.
        /// </summary>
        [Test]
        public void Test_SequenceController_Constructor_Test()
        {
            this.sequenceController = new SequenceController(this.mockSequenceBusiness.Object);
            Assert.AreEqual(typeof(SequenceController), this.sequenceController.GetType());
        }

        /// <summary>
        /// The controller get sequence test.
        /// </summary>
        [Test]
        public void FizzBuzz_Post_Input_Test()
        {
            var result = this.sequenceController.FizzBuzz(new FizzBuzzSequence { SequenceNumber = 5 });

            Assert.IsNotNull(result, "Results are null,Expected to get non nullable object");
            Assert.IsInstanceOf(typeof(ViewResult), result);
        }

        /// <summary>
        /// The controller get page test.
        /// </summary>
        [Test]
        public void FizzBuzz_Get_Pagination_Test()
        {
            // set cache value.
            HttpContext.Current.Cache.Insert("SequenceNumber", this.GetFakeSequences());

            var result = this.sequenceController.GetPage(2);

            Assert.IsNotNull(result, "Results are null,Expected to get non nullable object");
            Assert.IsInstanceOf(typeof(ViewResult), result);
        }

        /// <summary>
        /// The fizz buzz_ get_ pagination_ with cache null_ test.
        /// </summary>
        [Test]
        public void FizzBuzz_Get_Pagination_WithCacheNull_Test()
        {
            // remove existing cache values
            HttpContext.Current.Cache.Remove("SequenceNumber");

            var result = (RedirectToRouteResult)this.sequenceController.GetPage(2);

            Assert.IsNotNull(result, "Results are null,Expected to get non nullable object");
            Assert.That(result.RouteValues["action"], Is.EqualTo("Index"));
        }

        #region Test Initialize

        /// <summary>
        /// The Initialize.
        /// </summary>
        [SetUp]
        public void Initialize()
        {
            // Mock Business Entities
            this.mockSequenceBusiness = new Mock<ISequenceBusiness>();
            this.mockSequenceBusiness.Setup(x => x.GetSequence(It.IsAny<int>())).Returns(this.GetFakeSequences());

            // Create httpcontext.current
            var mockUrl = "http://mydomain.com";
            HttpContext.Current = new HttpContext(new HttpRequest(string.Empty, mockUrl, string.Empty), new HttpResponse(new StringWriter()));

            // read from here, if fails to read from appconfig file.
            ConfigurationManager.AppSettings["MaximumPage"] = "20";
            ConfigurationManager.AppSettings["MinimumPage"] = "1";

            this.sequenceController = new SequenceController(this.mockSequenceBusiness.Object);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// The fake sequences.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        private List<string> GetFakeSequences()
        {
            return new List<string> { "1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11" };
        }
        #endregion
    }
}
